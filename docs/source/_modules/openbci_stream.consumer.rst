openbci\_stream.consumer package
================================

.. automodule:: openbci_stream.consumer
   :members:
   :no-undoc-members:
   :no-show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   openbci_stream.consumer.openbci_consumer
