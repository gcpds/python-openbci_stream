openbci\_stream package
=======================

.. automodule:: openbci_stream
   :members:
   :no-undoc-members:
   :no-show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   openbci_stream.acquisition
   openbci_stream.consumer
   openbci_stream.handlers
   openbci_stream.preprocess
   openbci_stream.utils

Submodules
----------

.. toctree::
   :maxdepth: 4

   openbci_stream.doc_urls
