openbci\_stream.handlers package
================================

.. automodule:: openbci_stream.handlers
   :members:
   :no-undoc-members:
   :no-show-inheritance:

Submodules
----------

.. toctree::
   :maxdepth: 4

   openbci_stream.handlers.eegframe
   openbci_stream.handlers.hdf5_handler
