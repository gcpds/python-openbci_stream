.. include:: _notebooks/readme.rst


Navigation
^^^^^^^^^^

.. toctree::
   :maxdepth: 2
   :name: mastertoc

   _notebooks/01-hardware_configurations
   _notebooks/02-kafka_configuration
   _notebooks/03-openbci_stream_installation
   _notebooks/04-data_acquisition
   _notebooks/05-board_modes
   _notebooks/06-electrodes_impedance
   _notebooks/07-stream_markers
   _notebooks/08-eeg_processing
   _notebooks/09-eeg_filtering

   _notebooks/A1-raw_cleaning
   _notebooks/A2-binary_deserialization
   _notebooks/A3-command_line_interface
   _notebooks/A4-configure_remote_host
   _notebooks/A5-mne_processing
   _notebooks/A6-data_storage_handler




Indices and tables
^^^^^^^^^^^^^^^^^^

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



----

This project is licenced under the `Simplified BSD License <_notebooks/license.html>`_
