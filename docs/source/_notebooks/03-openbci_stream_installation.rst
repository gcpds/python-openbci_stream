Installation
============

GNU/Linux installation
----------------------

.. note::

   We recommend **Python 3.8** over **3.7**

Is recommended to deploy a development environ using
`venv <https://docs.python.org/3/library/venv.html>`__, after that,
``openbci`` can be instaled from
`PyPi <https://pypi.org/project/openbci/>`__.

.. code:: ipython3

    $ python3 -m venv openbci_venv
    $ source openbci_venv/bin/activate
    $ pip install -U openbci-stream

Windows
-------

Who cares
