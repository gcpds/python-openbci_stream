from kafka import KafkaProducer
import numpy as np
import time
from datetime import datetime
import pickle

producer_eeg = KafkaProducer(bootstrap_servers=['localhost:9092'],
                                      # compression_type='gzip',
                                 value_serializer=lambda x: pickle.dumps(x),
                                 )


CHRS = np.array(range(ord('A'), ord('Z') + 1))


# N = 1.024
N = 5 / (2.048e6/2**21)
# N = (2.048e6/2**21)

t0 = time.time()
while True:
    # marker = CHRS[0]
    # print(marker)
    # while t0 + N > time.time():
    time.sleep(N-0.0022)
    producer_eeg.send('marker', {'timestamp': datetime.now().timestamp(), 'marker': 'PINGUINO'})
    # t0 = time.time()

    # CHRS = np.roll(CHRS, -1)

