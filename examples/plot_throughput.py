"""
===============
Plot throughput
===============
"""

from matplotlib import pyplot as plt
plt.ion()
plt.subplots_adjust(hspace=0.3)



from openbci_stream.consumer import OpenBCIConsumer

import logging
logging.getLogger().setLevel(logging.INFO)

from datetime import datetime
import time

binary_created = []
packed = []
stream_ = []
instantant_samples = []
historical_samples = []

K = 0
samples = 0
# with OpenBCIConsumer(host='192.168.1.1', sample_rate='SAMPLE_RATE_250SPS') as stream:
with OpenBCIConsumer('wifi', '192.168.1.113', host='192.168.1.1', stream_samples=1000) as (stream, openbci):
    openbci.command(openbci.SAMPLE_RATE_2KSPS)


    # SAMPLE_RATE_2KSPS

    t0 = time.time()
    tp = time.time()
    for data in stream:

        samples += data.value['samples']

        binary_created.append((datetime.now() - datetime.fromtimestamp(data.value['binary_created'])).total_seconds())
        packed.append((datetime.now() - datetime.fromtimestamp(data.value['created'])).total_seconds())
        stream_.append((datetime.now() - datetime.fromtimestamp(data.timestamp/1000)).total_seconds())

        t = time.time() - t0
        tpp = time.time() - tp
        t0 = time.time()

        instantant_samples.append(data.value['samples']/t)
        historical_samples.append(samples/tpp)

        plt.subplot(211)
        plt.cla()
        plt.title('Latency')
        plt.grid(True)
        plt.plot(binary_created[K:], label='Binary created')
        plt.plot(packed[K:], label='Packed')
        plt.plot(stream_[K:], label='Streamed')
        plt.ylabel('Time [$s$]')
        plt.xlabel('Samples set')
        plt.legend()

        plt.subplot(212)
        plt.cla()
        plt.title('Frequencies')
        plt.grid(True)
        plt.plot(instantant_samples[K:], label='Frequency')
        plt.plot(historical_samples[K:], label='Mean frequency')
        plt.ylabel('Samples per second [$SPS$]')
        plt.xlabel('Samples set')
        plt.legend()

        plt.draw()
        plt.pause(1e-15)



